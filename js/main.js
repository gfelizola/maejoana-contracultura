var currentArea    = "home",
	currentSubArea = "";

$(function() {
	$("header .logo").click(function(event) {
		event.preventDefault();
		currentArea = "";

		$("#site-container").removeClass().addClass("home");
		$("#menu ul li a").removeClass('ativo');
		$(".sub-area").removeClass('ativo');
	});

	$("header .bt-anos").click(function(event) {
		event.preventDefault();
		var areaId = $(this).attr("href");
		currentArea = areaId;

		$("#site-container").removeClass().addClass( areaId.substr(1) );
		$("#menu ul li a").removeClass('ativo').first().addClass('ativo');
		$(areaId + " .sub-area").removeClass('ativo').first().addClass('ativo');
	});

	$("#menu ul li a").click(function(event) {
		event.preventDefault();

		currentSubArea = $(this).attr("href").substr(1);

		$( currentArea + " .sub-area").removeClass('ativo');
		$( currentArea + " ." + currentSubArea).addClass('ativo').scrollTop(0);

		$("#menu ul li a").removeClass('ativo');
		$(this).addClass('ativo')
	});

	$(".bt-fancy").fancybox({
		padding      : 0,
		scrolling    : "no",
		parent       : "#site-container",
		topRatio     : 0.36,
		height: 768,
    	helpers      : {
    		title    : {
    			type : 'outside'
    		},
    		overlay: {
    			fixed: false
    		}
    	}
    });

  //   $(".bt-fancy-modal").fancybox({
		// padding       : 0,
		// scrolling     : "no",
		// parent        : "#site-container",
		// topRatio      : 0.36,
		// width         : 640,
		// height        : 400,
		// autoSize      : false,
  //   	helpers       : {
  //   		title     : {
  //   			type  : 'outside'
  //   		},
  //   		overlay   : {
  //   			fixed : false
  //   		}
  //   	},
  //   	afterShow: function(){
  //   		var vid      = this.content[0].id,
  //   			poster   = $(this.content).find("video").attr("poster"),
  //   			videoSrc = $(this.content).find("video source").attr("src");

  //   		// console.log( this.content[0].id, video );
  //   		_capa.video({
		// 		id       : vid,
		// 		mp4      : videoSrc,
		// 		poster   : poster,
		// 		controls : true,
		// 		width    : 640,
		// 		height   : 360
		// 	});

		// 	_capa.controls({
		// 		id: vid,
		// 		play: true
		// 	});
  //   	}
  //   });


	$(".bt-fancy-gal").fancybox({
		padding        : 0,
		parent         : "#site-container",
		topRatio       : 0.36,
		arrows         : false,
    	helpers        : {
    		title      : {
    			type   : 'outside'
    		},
    		thumbs     : {
				width  : 100,
				height : 70
			},
    		overlay: {
    			fixed: false
    		}
    	}
    });

	$(".bt-fancy-modal").click(function(event) {
		event.preventDefault();

		var vid      = $(this).attr('href').substr(1) + "-player",
  			poster   = $("#"+vid).attr("poster"),
  			videoSrc = $("#"+vid).find("source").attr("src");

  		// console.log( vid, videoSrc, poster, $("#"+vid) );
  		_capa.video({
			id       : "#" + vid + "-capa",
			mp4      : videoSrc,
			poster   : poster,
			controls : true,
			width    : 640,
			height   : 360,
			fullscreen: true
		});

		_capa.controls({
			id       : vid + "-capa",
			player   : "#"+vid + "-capa",
			play     : true,
			controls : true
		});
	});

    // $('video,audio').mediaelementplayer({
    // 	features: ['playpause','progress','current','duration'],
    // 	alwaysShowControls : true,
    // 	enableKeyboard     : false,
    // 	defaultVideoWidth  : 640,
    // 	defaultVideoHeight : 360
    // });

	// videos
	// _capa.video([{
	// 	id: "#musica-01-61",
	// 	mp4: "assets/video/video1.mp4",
	// 	controls: true
	// },{
	// 	id: "#musica-02-61",
	// 	mp4: "assets/video/video1.mp4",
	// 	controls: true
	// },{
	// 	id: "#musica-04-61",
	// 	mp4: "assets/video/video1.mp4",
	// 	controls: true
	// },{
	// 	id: "#cinema-01-61",
	// 	mp4: "assets/video/video1.mp4",
	// 	controls: true
	// },{
	// 	id: "#cinema-02-61",
	// 	mp4: "assets/video/video1.mp4",
	// 	controls: true
	// },{
	// 	id: "#musica-01-67",
	// 	mp4: "assets/video/video1.mp4",
	// 	controls: true
	// },{
	// 	id: "#musica-03-67",
	// 	mp4: "assets/video/video1.mp4",
	// 	controls: true
	// },{
	// 	id: "#cinema-01-67",
	// 	mp4: "assets/video/video1.mp4",
	// 	controls: true
	// },{
	// 	id: "#politica-01-67",
	// 	mp4: "assets/video/video1.mp4",
	// 	controls: true
	// }]);

	// _capa.controls();


});